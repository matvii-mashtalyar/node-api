const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const {
  createFighterValid,
  updateFighterValid,
} = require('../middlewares/fighter.validation.middleware');
const isEmpty = require('lodash.isempty');

const router = Router();

router.get('/', (req, res) => {
  const fighters = FighterService.getAllFighters();
  if (isEmpty(fighters)) {
    res.status(404).json({ error: true, message: 'Fighters not found' });
  } else {
    res.send(fighters);
  }
});

router.get('/:id', responseMiddleware, (req, res) => {
  if (res.statusCode === 404) {
    return;
  } else {
    res.send(FighterService.search({ id: req.params.id }));
  }
});

router.post('/', createFighterValid, (req, res) => {
  if (res.statusCode === 400) {
    return;
  }

  const createdFighter = FighterService.create(req.body);
  res.status(200).send(createdFighter);
});

router.put('/:id', updateFighterValid, (req, res) => {
  if (res.statusCode === 400 || res.statusCode === 404) {
    return;
  }

  const updatedFighter = FighterService.update(req.params.id, req.body);
  res.status(200).send(updatedFighter);
});

router.delete('/:id', responseMiddleware, (req, res) => {
  if (res.statusCode === 404) {
    return;
  } else {
    const deletedFighter = FighterService.delete(req.params.id);
    res.status(200).send(deletedFighter);
  }
});

module.exports = router;
