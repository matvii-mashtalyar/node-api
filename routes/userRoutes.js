const { Router } = require('express');
const UserService = require('../services/userService');
const {
  createUserValid,
  updateUserValid,
} = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');
const isEmpty = require('lodash.isempty');

const router = Router();

router.get('/', (req, res) => {
  const users = UserService.getAllUsers();
  if (isEmpty(users)) {
    res.status(404).json({ error: true, message: 'Users not found' });
  } else {
    res.send(users);
  }
});

router.get('/:id', responseMiddleware, (req, res) => {
  if (res.statusCode === 404) {
    return;
  } else {
    res.send(UserService.search({ id: req.params.id }));
  }
});

router.post('/', createUserValid, (req, res) => {
  if (res.statusCode === 400) {
    return;
  }

  const createdUser = UserService.create(req.body);
  res.status(200).send(createdUser);
});

router.put('/:id', updateUserValid, (req, res) => {
  if (res.statusCode === 400 || res.statusCode === 404) {
    return;
  }

  const updatedUser = UserService.update(req.params.id, req.body);
  res.status(200).send(updatedUser);
});

router.delete('/:id', responseMiddleware, (req, res) => {
  if (res.statusCode === 404) {
    return;
  } else {
    const deletedUser = UserService.delete(req.params.id);
    res.status(200).send(deletedUser);
  }
});

module.exports = router;
