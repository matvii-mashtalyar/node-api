const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
  getAllFighters() {
    const fighters = FighterRepository.getAll();
    return fighters;
  }

  search(search) {
    const item = FighterRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  create(data) {
    const createdFighter = FighterRepository.create(data);
    return createdFighter;
  }

  update(id, dataToUpdate) {
    const updatedFighter = FighterRepository.update(id, dataToUpdate);
    return updatedFighter;
  }

  delete(id) {
    const deletedFighter = FighterRepository.delete(id);
    return deletedFighter;
  }
}

module.exports = new FighterService();
