const { UserRepository } = require('../repositories/userRepository');

class UserService {
  getAllUsers() {
    const users = UserRepository.getAll();
    return users;
  }

  search(search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  create(data) {
    const createdUser = UserRepository.create(data);
    return createdUser;
  }

  update(id, dataToUpdate) {
    const updatedUser = UserRepository.update(id, dataToUpdate);
    return updatedUser;
  }

  delete(id) {
    const deletedUser = UserRepository.delete(id);
    return deletedUser;
  }
}

module.exports = new UserService();
