const { expect } = require('chai');
const { user } = require('../models/user');
const UserService = require('../services/userService');

const createUserValid = (req, res, next) => {
  const props = Object.keys(user).slice(1);
  try {
    expect(req.body).to.not.have.property('id');
    expect(req.body).to.have.all.keys(props);
    expect(req.body.email).to.match(/^[\w-\.]+@gmail.com$/);
    expect(req.body.phoneNumber).to.match(/^\+380+[0-9]{9}$/);
    expect(req.body.password).to.be.a('string').that.have.lengthOf.at.least(3);
    if (
      UserService.search({ email: req.body.email }) ||
      UserService.search({ phoneNumber: req.body.phoneNumber })
    ) {
      res.status(400).json({
        error: true,
        message: 'User with this email or phone already exists',
      });
    }
  } catch (err) {
    res.status(400).json({ error: true, message: `${err}` });
  }

  next();
};

const updateUserValid = (req, res, next) => {
  const props = Object.keys(user).slice(1);
  try {
    if (!UserService.search({ id: req.params.id })) {
      res.status(404).json({
        error: true,
        message: 'User not found',
      });
      return;
    }
    expect(req.body).to.not.have.property('id');
    expect(req.body).to.have.any.keys(props);
    if (req.body.email) {
      expect(req.body.email).to.match(/^[\w-\.]+@gmail.com$/);
    }
    if (req.body.phoneNumber) {
      expect(req.body.phoneNumber).to.match(/^\+380+[0-9]{9}$/);
    }
    if (req.body.password) {
      expect(req.body.password)
        .to.be.a('string')
        .that.have.lengthOf.at.least(3);
    }
  } catch (err) {
    res.status(400).json({ error: true, message: `${err}` });
  }

  next();
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
