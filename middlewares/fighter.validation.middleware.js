const { fighter } = require('../models/fighter');
const FighterService = require('../services/fighterService');
const { expect } = require('chai');

const createFighterValid = (req, res, next) => {
  try {
    expect(req.body, 'fighter').to.have.property('name');
    if (
      FighterService.search((fighter) => {
        return fighter.name.toLowerCase() === req.body.name.toLowerCase();
      })
    ) {
      res.status(400).json({
        error: true,
        message: 'Fighter with same name already exists',
      });
      next();
    }
    expect(req.body, 'fighter').to.not.have.property('id');
    expect(req.body, 'fighter').to.have.property('power');
    expect(req.body, 'fighter').to.have.property('defense');
    expect(req.body.power, 'power').to.be.a('number').above(1).and.below(100);
    expect(req.body.defense, 'defense')
      .to.be.a('number')
      .above(1)
      .and.below(10);
    if (req.body.health) {
      expect(req.body.health, 'health')
        .to.be.a('number')
        .above(80)
        .and.below(120);
    } else {
      req.body.health = 100;
    }
  } catch (err) {
    res.status(400).json({ error: true, message: `${err}` });
  }
  next();
};

const updateFighterValid = (req, res, next) => {
  try {
    expect(req.body, 'fighter').to.not.have.property('id');
    expect(req.body, 'fighter').to.have.any.keys('name', 'power', 'defense');
    if (req.body.power) {
      expect(req.body.power, 'power').to.be.a('number').above(1).and.below(100);
    }
    if (req.body.defense) {
      expect(req.body.defense, 'defense')
        .to.be.a('number')
        .above(1)
        .and.below(10);
    }
    if (req.body.health) {
      expect(req.body.health, 'health')
        .to.be.a('number')
        .above(80)
        .and.below(120);
    }
    if (!FighterService.search({ id: req.params.id })) {
      res.status(404).json({ error: true, message: 'Fighter not found' });
      next();
    }
  } catch (err) {
    res.status(400).json({
      error: true,
      message: `${err}`,
    });
  }
  next();
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
