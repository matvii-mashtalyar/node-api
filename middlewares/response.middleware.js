const UserService = require('../services/userService');
const FighterService = require('../services/fighterService');
const AuthService = require('../services/authService');

const responseMiddleware = (req, res, next) => {
  switch (req.baseUrl) {
    case '/api/users':
      const findedUser = UserService.search({ id: req.params.id });
      if (findedUser === null) {
        res.status(404).json({ error: true, message: 'User not found' });
      }
      next();
      break;

    case '/api/fighters':
      const findedFighter = FighterService.search({ id: req.params.id });
      if (findedFighter === null) {
        res.status(404).json({ error: true, message: 'Fighter not found' });
      }
      next();
      break;
    case '/api/auth':
      if (res.err) {
        res.status(401).json({ error: true, message: 'Invalid credentials' });
      } else {
        res.status(200).send(res.data);
      }

      break;
  }
};

exports.responseMiddleware = responseMiddleware;
